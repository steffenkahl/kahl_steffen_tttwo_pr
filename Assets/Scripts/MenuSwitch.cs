using UnityEngine;
using UnityEngine.SceneManagement;

namespace TTTWO_PR
{
    //A script for switching to the main menu if a specific method gets called
    public class MenuSwitch : MonoBehaviour
    {
        [SerializeField] private string mainMenuScene; //The name of the main menu to make it changeable from the inspector

        public void CloseLevel()
        {
            //If this method gets called, switch to the specified main menu
            SceneManager.LoadScene(mainMenuScene, LoadSceneMode.Single);
        }
    }
}