using UnityEngine;

namespace TTTWO_PR
{
    //Script that moves the tornado depending on the distance to the player
    public class TornadoMover : MonoBehaviour
    {
        [SerializeField] private float closeDistance; //At what distance the tornado changes speeds
        [SerializeField] private float farSpeed; //How fast the tornado should go when its far away of the player
        [SerializeField] private float closeSpeed; //How fast the tornado should go when its near the player
        private float currentSpeed; //The currently used speed
        private GameObject player; //A reference to the player

        private void Start()
        {
            player = GameObject.FindWithTag(GameTags.PLAYER);
            currentSpeed = closeSpeed;
        }

        private void Update()
        {
            //Checks the distance to the player and changes speeds accordingly
            if (Vector3.Distance(transform.position, player.transform.position) <= closeDistance)
            {
                currentSpeed = closeSpeed; //Switch to slower speed if the player is nearer
            }
            else
            {
                currentSpeed = farSpeed; //Switch to faster speed if the player is far away
            }

            transform.Translate(Vector3.right * currentSpeed); //move the tornado
        }
    }
}