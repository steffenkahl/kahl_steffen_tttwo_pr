using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TTTWO_PR
{
    //A PlayerController for the main character
    public class PlayerController : MonoBehaviour
    {
        //Some Serialized Variables for controls in the inspector
        [SerializeField] private float gravity; // How fast the character should be pulled downwards
        [SerializeField] private float jumpHeight; //How High the character jumps when pressing the jump key
        [SerializeField] private float speed; //How fast the character moves
        [SerializeField] private float jumpSmoother; //How smooth the jump speed should decrease
        [SerializeField] private bool sideView; //if the sideview is active
        [SerializeField] private CinemachineVirtualCamera sideCamera; //A reference to the cinemachine positioned on the side of the character
        [SerializeField] private CinemachineVirtualCamera backCamera; //A reference to the cinemachine positioned behind the character

        private Camera mainCamera; //A reference to the main menu
        private PlayerInput playerInput; //A reference to the PlayerInput Component
        private CharacterController characterController; //A reference to the CharacterController Component
        private Vector3 jump; //Vector 3 which describes the jump vector
        private float directionSide; //What direction the character should go sideways
        private float directionDepth; //What direction the character should go front to back
        private Vector3 motion; //A variable for the final computed direction

        //Start gets called at the beginning of the game
        private void Start()
        {
            //First get references
            characterController = GetComponent<CharacterController>();
            playerInput = GetComponent<PlayerInput>();
            mainCamera = Camera.main;
            
            //Then set the starting view
            sideView = true;
        }

        //FixedUpdate gets called at a fixed intervall
        private void FixedUpdate()
        {
            //First check if a jump is active and if it is, smooth it out
            if (jump.y > 0) //If the jumpVector is not 0 it should decrease by the jumpSmooth-value
            {
                jump.y -= jumpSmoother;
            }
            else
            {
                jump.y = 0f;
            }

            //Check the view and if its the sideview, control the z-axis of the player
            if (sideView)
            {
                characterController.enabled = false; //You need to disable the characterController to move the player
                transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
                characterController.enabled = true;
            }

            //Lastly create a motion-vector and move the charactercontroller
            motion = (directionSide * Vector3.right * speed) + (directionDepth * Vector3.back * speed) + jump + (gravity * Vector3.up);
            characterController.Move(motion * Time.deltaTime);
        }

        //Update gets called once every frame
        private void Update()
        {
            //Check the cameras to switch their priorities if necessary
            UpdateCameras();
        }

        private void UpdateCameras()
        {
            //Check which view is active and switch priorities of the cinemachines
            if (sideView)
            {
                sideCamera.Priority = 2;
                backCamera.Priority = 1;
            }
            else
            {
                sideCamera.Priority = 1;
                backCamera.Priority = 2;
            }
        }

        #region InputActions
        //Methods that get called through InputActions in the PlayerInput Component
        public void OnMovementSideways(InputAction.CallbackContext value)
        {
            directionSide = value.ReadValue<float>();
        }

        public void OnMovementFrontBack(InputAction.CallbackContext value)
        {
            directionDepth = value.ReadValue<float>();
        }

        public void OnJump(InputAction.CallbackContext value)
        {
            if (characterController.isGrounded)
            {
                Debug.Log("is Jumping");
                jump = new Vector3(0f, jumpHeight, 0f);
            }
        }

        public void OnPerspectiveChange(InputAction.CallbackContext value)
        {
            if (value.started)
            {
                //Switch the camera-variable but also change the actionmap and reset the z-axis if necessary
                sideView = !sideView;
                if (sideView)
                {
                    playerInput.SwitchCurrentActionMap(playerInput.actions.FindActionMap("SideView").name);
                    mainCamera.orthographic = true;
                    characterController.enabled = false; //You need to disable the characterController to move the player
                    transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
                    characterController.enabled = true;
                }
                else
                {
                    playerInput.SwitchCurrentActionMap(playerInput.actions.FindActionMap("BackView").name);
                    mainCamera.orthographic = false;
                }
            }
        }
        #endregion
    }
}