﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace TTTWO_PR
{
    //Script for killing the player on contact with a death trigger
    public class DeathTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            //If an object triggers, check if it is a player
            if (other.CompareTag(GameTags.PLAYER))
            {
                //If so, reload the current level
                SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
            }
        }
    }
}