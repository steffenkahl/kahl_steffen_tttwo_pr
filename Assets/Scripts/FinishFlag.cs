using UnityEngine;
using UnityEngine.SceneManagement;

namespace TTTWO_PR
{
    //Script for finishing a level if the player triggers a finish flag
    public class FinishFlag : MonoBehaviour
    {
        [SerializeField] private string MainMenuScene; //The name of the main menu to make it changeable from the inspector

        private void OnTriggerEnter(Collider other)
        {
            //If an object triggers, check if it is a player
            if (other.CompareTag(GameTags.PLAYER))
            {
                //If so, load the given MainMenuScene
                SceneManager.LoadScene(MainMenuScene, LoadSceneMode.Single);
            }
        }
    }
}