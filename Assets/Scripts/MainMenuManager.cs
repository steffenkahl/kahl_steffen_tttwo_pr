using UnityEngine;
using UnityEngine.SceneManagement;

namespace TTTWO_PR
{
    //A script for the main menu. All methods get called from buttons.
    public class MainMenuManager : MonoBehaviour
    {
        public void LoadLevel1()
        {
            //Load Level 1 if the corresponding button triggers this method
            SceneManager.LoadScene("Level1", LoadSceneMode.Single);
        }
        
        public void LoadLevel2()
        {
            //Load Level 2 if the corresponding button triggers this method
            SceneManager.LoadScene("Level2", LoadSceneMode.Single);
        }
        
        public void LoadLevel3()
        {
            //Load Level 3 if the corresponding button triggers this method
            SceneManager.LoadScene("Level3", LoadSceneMode.Single);
        }
        
        public void LoadLevelShaderShowcase()
        {
            //Load the ShaderShowcase-Level if the corresponding button triggers this method
            SceneManager.LoadScene("ShaderShowcase", LoadSceneMode.Single);
        }
        
        public void QuitGame()
        {
            //Quit the game on the "Quit Game"-Button
            Debug.Log("Game is quitting"); //Send a debug log for the testing in the editor
            Application.Quit();
        }
    }
}